import csv
from random import shuffle


def str_shuffle(string):
    temp = list(string)
    shuffle(temp)
    return ''.join(temp)

file2 = open("data/dataset/dev.csv",encoding='UTF-8-sig')
reader = csv.reader(file2)
original = list(reader)
file2.close()
file1 = open("data/dataset/dev.csv", "w", newline="",encoding='UTF-8-sig')
content = csv.writer(file1)
temp = original[1:]
shuffle(temp)
content.writerow(original[0])
for row in temp:
    content.writerow(row)


file1.close()
