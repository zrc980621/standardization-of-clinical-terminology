from similarity import BertSim
import tensorflow as tf
from search import Search



def main():
    temp1 = 0
    sim = BertSim()
    mode = input("mode(train/eval/predict): ")
    if mode == 'train':
        sim.set_mode(tf.estimator.ModeKeys.TRAIN)
        sim.train()
    elif mode == 'eval':
        sim.set_mode(tf.estimator.ModeKeys.EVAL)
        sim.eval()
    elif mode == 'predict':
        while True:       
            original = input('original: ')
            probable = Search(original)
            print(probable)
            sim.set_mode(tf.estimator.ModeKeys.PREDICT)
            for each in probable:
                predict = sim.predict(original, each)
                print(f'similarity：{predict[0][1]}')
                if temp1 < predict[0][1]:
                    temp1 = predict[0][1]
                    temp2 = each
            predict = sim.predict(original, temp2)
            print(f'similarity：{predict[0][1]}')



if __name__ == '__main__':
    main()