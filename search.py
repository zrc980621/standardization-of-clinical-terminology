from whoosh.qparser import QueryParser
from whoosh.qparser import MultifieldParser
from whoosh.qparser import FuzzyTermPlugin
from whoosh.qparser import PhrasePlugin
from whoosh.qparser import SequencePlugin
from whoosh.query import FuzzyTerm
from whoosh import scoring
from whoosh.index import open_dir
from whoosh import index    
from whoosh.fields import *
from index import CreatIndex
import pkuseg


seg = pkuseg.pkuseg(model_name='medicine')

def Search(sentence):
    output = []
    words = seg.cut(sentence) 
    word = ""
    for each in words:
        word += each
        numb = len(each)
        if numb <= 4:
            word += ("~2")
        elif 4 < numb <= 6:
            word += ("~3")
        elif 6 < numb:
            word += ("~4")
    exists1 = index.exists_in("indexdir", indexname='standardization_index')
    exists2 = index.exists_in("indexdir", indexname='icd_index')
    if exists1 == exists2 == False:
        CreatIndex()
    ix = open_dir("indexdir", indexname='standardization_index')
    iz = open_dir("indexdir", indexname='icd_index')
    with ix.searcher(weighting=scoring.MultiWeighting(scoring.BM25F(), id=scoring.Frequency(), keys=scoring.TF_IDF())) as searcher:
        parser = QueryParser("Original", ix.schema)
        parser.add_plugin(FuzzyTermPlugin)
        query = parser.parse(word)
        results = searcher.search(query, limit=50)
        print(results)
        if len(results):
            for each in results[:]:
                output.append(each["Standardization"])
    with iz.searcher(weighting=scoring.MultiWeighting(scoring.BM25F(), id=scoring.Frequency(), keys=scoring.TF_IDF())) as searcher:
        parser = QueryParser("Icd", iz.schema)
        parser.add_plugin(FuzzyTermPlugin)
        query = parser.parse(word)
        results = searcher.search(query, limit=50)
        print(results)
        if len(results):
            for each in results[:]:
                output.append(each["Standardization"])
    return output
