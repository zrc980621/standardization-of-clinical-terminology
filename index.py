from whoosh import index    
from whoosh.index import create_in
from whoosh.fields import *
from whoosh.analysis import Tokenizer,Token 
import os.path
import csv
import pkuseg


seg = pkuseg.pkuseg(model_name='medicine', user_dict="/data/dict.txt")
schema = HisSchema()
schema1 = IcdSchema()

class ChineseTokenizer(Tokenizer):  
    def __call__(self, value, positions=False, chars=False, keeporiginal=False, removestops=True, start_pos=0, start_char=0, mode='', **kwargs):  
        assert isinstance(value, text_type), "%r is not unicode" % value  
        t = Token(positions, chars, removestops=removestops, mode=mode, **kwargs)  
        #seglist=jieba.cut_for_search(value)
        seglist=iter(seg.cut(value))      
        for w in seglist:  
            t.original = t.text = w  
            t.boost = 1.0  
            if positions:  
                t.pos=start_pos+value.find(w)  
            if chars:  
                t.startchar=start_char+value.find(w)  
                t.endchar=start_char+value.find(w)+len(w)  
            yield t    

def ChineseAnalyzer():  
    return ChineseTokenizer()

analyzer = ChineseAnalyzer()

class HisSchema(SchemaClass):
    Original = TEXT(stored=True, analyzer=analyzer)
    Standardization = TEXT(stored=True, analyzer=analyzer)

class IcdSchema(SchemaClass):
    Icd = TEXT(stored=True, analyzer=analyzer)
    Standardization = TEXT(stored=True, analyzer=analyzer)

# class HisSchema(SchemaClass):
#     Original = TEXT(stored=True)
#     Standardization = TEXT(stored=True)

# class IcdSchema(SchemaClass):
#     Icd = TEXT(stored=True)
#     Standardization = TEXT(stored=True)




def CreatIndex():
    if not os.path.exists("indexdir"):
        os.mkdir("indexdir")

    ix = create_in("indexdir", schema, indexname='standardization_index')
    iz = create_in("indexdir", schema1, indexname='icd_index')
    num = 0
    writer = ix.writer()
    with open("data/single.txt","r",encoding='UTF-8-sig') as f:
        line = f.readline()
        while line: 
            words = line.split('\t')
            writer.add_document(Original=words[0], Standardization=words[1])                 
            line = f.readline()
            print(num)
            num+=1
    writer.commit()
    writer = iz.writer()
    with open('data/ICD_10v601.csv','r',encoding='UTF-8-sig') as csvfile:
        reader = csv.reader(csvfile)
        column1 = [row[1]for row in reader]
        for each in column1:
            writer.add_document(Icd=each, Standardization=each)
            print(num)
            num+=1             
    writer.commit()

